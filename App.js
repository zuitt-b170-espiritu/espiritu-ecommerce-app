// Base Imports
import React, {useState} from 'react';
import {BrowserRouter as Router,Route, Routes} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';


// App Components
import AppNavbar from './components/AppNavbar.js';


// Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';




export default function App() {
// localStorage.getItem - used to retrieve a piece or the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.setItem in the Login.js
  const [ user, setUser ] = useState( { accessToken: localStorage.getItem('access') } );

  const unsetUser = () => {
    //localStorage.clear() - to clear every information that is stored inside the localStorage
    localStorage.clear();
    setUser( { access: null } )
  }



/*
  path="*" - all other unspecified routes. this is to make sure that all other routes, beside the ones in the return statement, will render the Error page.
*/

/*
  The Provider component inside useContext is what allows other components to consume or use the context. Any component which is not wrapped by the Provider will have access to the values provided in the context.
*/

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
       <Router>
         <AppNavbar user={user} />
         <Routes>
             <Route path = "/" element={<Home />} />
             <Route path = "/courses" element = {<Courses />} />
             <Route path = "/register" element = {<Register />} />
             <Route path = "/login" element = {<Login />} />
             <Route path="*" element = {<Error />} />
         </Routes>
      </Router>
    </UserContext.Provider>
    )
}
